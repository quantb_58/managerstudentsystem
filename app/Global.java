import com.avaje.ebean.Ebean;
import play.*;
import models.*;
import play.api.mvc.EssentialFilter;
import play.filters.csrf.CSRFFilter;
import play.filters.gzip.GzipFilter;
import play.GlobalSettings;
import play.libs.F.Promise;
import play.libs.Yaml;
import play.mvc.Http.RequestHeader;
import java.util.List;
import java.util.Map;
import static play.mvc.Results.notFound;

public class Global extends GlobalSettings {

    public void onStart(Application app) {
        Logger.info("Application has started");
        InitialData.insert(app);
    }

    static class InitialData {
        public static void insert(Application app) {
            Map<String, List<Object>> all =
                    (Map<String, List <Object>>) Yaml.load("initial-data.yml");
            if (Ebean.find(Tag.class).findRowCount() == 0) {
                Ebean.save(all.get("tags"));
            }

            if (Ebean.find(UserAccount.class).findRowCount() == 0) {
                Ebean.save(all.get("user_accounts"));
            }
        }
    }

    public void onStop(Application app) {
        Logger.info("Application shutdown...");
    }

    public <T extends EssentialFilter> Class<T>[] filters() {
        return new Class[]{CSRFFilter.class, GzipFilter.class};
    }
}