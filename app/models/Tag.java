package models;

import play.data.validation.Constraints;
import javax.persistence.*;
import java.util.*;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

/**
 * Created by Administrator on 2/6/2015.
 */
@Entity
public class Tag extends Model{
    @Id
    public Long id;
    @Constraints.Required
    public String name;
    @ManyToMany(mappedBy="tags")
    public List<Student> students;

    @ManyToMany(mappedBy="tags")
    public List<Teacher> teachers;
    public Tag() {
        //Left empty
    }

    public Tag(Long id, String name, Collection<Student> students) {
        this.id = id;
        this.name = name;
        this.students = new LinkedList<Student>(students);
        for (Student Student : students) {
            Student.tags.add(this);
        }
    }

//    public Tag(Long id,String name, Collection<Teacher> teachers) {
//        this.id = id;
//        this.name = name;
//        this.teachers = new LinkedList<Teacher>(teachers);
//        for (Teacher Teacher : teachers) {
//            Teacher.tags.add(this);
//        }
//    }
    public static Finder<Long, Tag> find = new Finder<>(Long.class, Tag.class);
    public static Tag findById(Long id) {
        return find.byId(id);
    }
}
