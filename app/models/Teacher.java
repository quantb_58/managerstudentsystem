package models;



import java.util.ArrayList;

import com.avaje.ebean.Page;
import play.data.validation.Constraints;

import play.data.format.Formats;
import java.util.LinkedList;
import java.util.List;
import java.util.Date;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.*;

@Entity
public class Teacher extends Model implements play.mvc.PathBindable<Teacher>{
    @Id
    public Long id;
    @Constraints.Required
    public String ean;
    @Constraints.Required
    public String name;

//    @Constraints.Required
    public String phone;
    public String faculty;
    public String classes;// classes = học vị
    @Constraints.Required
    public String email;
    @Constraints.Required
    public String password;
    @Constraints.Required
    public String confirm_password;

    public byte[] picture;
    public Teacher() {}

    public Teacher(String name,String phone, String faculty, String classes, String email) {
        this.ean = ean;
        this.name = name;

        this.phone = phone;
        this.faculty = faculty;
        this.classes = classes;
        this.email = email;
    }
    public String toString() {
        return String.format("%s ",name);
    }
    public static Teacher authenticate(String email, String password) {
        return finder.where().eq("email", email).eq("password", password).findUnique();
    }

    public static Finder<Long, Teacher> finder =
            new Finder<Long, Teacher>(Long.class, Teacher.class);
    private static List<Teacher> teachers;

    public static Finder<Long,Teacher> find = new Finder<Long,Teacher>(Long.class, Teacher.class);

    public static List<Teacher> findAll() {
        return find.all();
    }
    public static Page<Teacher> find(int page) {  // trả về trang thay vì List
        return find.where()
                .orderBy("id asc")     // sắp xếp tăng dần theo id
                .findPagingList(5)    // quy định kích thước của trang
                .setFetchAhead(false)  // có cần lấy tất cả dữ liệu một thể?
                .getPage(page);    // lấy trang hiện tại, bắt đầu từ trang 0
    }
    public void delete() {
        for (Tag tag : tags) {
            tag.teachers.remove(this);
            tag.save();
        }
        super.delete();
    }
    public static Teacher findByEan(String ean) {
        return find.where().eq("ean",ean).findUnique();
    }
    public static Teacher findByEmail(String email) {
        return find.where().eq("email", email).findUnique();
    }
    public static List<Teacher> findByName(String term) {
        final List<Teacher> results = new ArrayList<Teacher>();
        for (Teacher candidate : teachers) {
            if (candidate.name.toLowerCase().contains(term.toLowerCase())) {
                results.add(candidate);
            }
        }
        return results;
    }
    public static int getTotalRowCount() {
        return find.where().findPagingList(5).getTotalRowCount();
    }
    @ManyToMany
    public List<Tag> tags = new LinkedList<Tag>();
    @Override
    public Teacher bind(String key, String value) {
        return findByEan(value);
    }

    @Override
    public String unbind(String key) {
        return ean;
    }

    @Override
    public String javascriptUnbind() {
        return ean;
    }
}