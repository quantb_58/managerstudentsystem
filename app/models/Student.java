package models;



import java.util.ArrayList;

import com.avaje.ebean.Page;
import play.data.validation.Constraints;

import play.data.format.Formats;
import java.util.LinkedList;
import java.util.List;
import java.util.Date;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.*;

@Entity
public class Student extends Model implements play.mvc.PathBindable<Student>{  
    @Id
    public Long id;
    @Constraints.Required
    public String ean;
    @Constraints.Required
    public String name;
    @Formats.DateTime(pattern = "yyyy-MM-dd")
    @Constraints.Required
    public Date birthday;
    @Constraints.Required
    public String address;
    @Constraints.Required
    public String phone;
    @Constraints.Required
    public String faculty;
    @Constraints.Required
    public String classes;
    @Constraints.Required
    public String email;
    @Constraints.Required
    public String password;
    @Constraints.Required
    public String confirm_password;
    @Lob
    public byte[] avatar;   
    public Student() {}

    public Student(String ean, String name, Date birthday, String address, String phone, String faculty, String classes, String email) {
        this.ean = ean;
        this.name = name;
        this.birthday = birthday;
        this.address = address;
        this.phone = phone;
        this.faculty = faculty;
        this.classes = classes;
        this.email = email;
    }
    public String toString() {
        return String.format("%s - %s", ean, name);
    }
    public static Student authenticate(String email, String password) {
        return finder.where().eq("email", email).eq("password", password).findUnique();
    }
    
    public static Finder<Long, Student> finder =
               new Finder<Long, Student>(Long.class, Student.class);
    private static List<Student> students;

    public static Finder<Long,Student> find = new Finder<Long,Student>(Long.class, Student.class);

    public static List<Student> findAll() {
        return find.all();
    }
    public static Page<Student> find(int page) {  // trả về trang thay vì List
        return find.where()
              .orderBy("id asc")     // sắp xếp tăng dần theo id
              .findPagingList(5)    // quy định kích thước của trang
              .setFetchAhead(false)  // có cần lấy tất cả dữ liệu một thể?
              .getPage(page);    // lấy trang hiện tại, bắt đầu từ trang 0
    }
    public void delete() {
        for (Tag tag : tags) {
            tag.students.remove(this);
            tag.save();
        }
        super.delete();
    }
    public static Student findByEan(String ean) {
       return find.where().eq("ean",ean).findUnique();
    }
    public static Student findByEmail(String email) {
        return find.where().eq("email", email).findUnique();
    }
    public static List<Student> findByName(String term) {
        final List<Student> results = new ArrayList<Student>();
        for (Student candidate : students) {
            if (candidate.name.toLowerCase().contains(term.toLowerCase())) {
                results.add(candidate);
            }
        }
        return results;
    }
    public static int getTotalRowCount() {
        return find.where().findPagingList(5).getTotalRowCount();
    }
    @ManyToMany
    public List<Tag> tags = new LinkedList<Tag>();
    @Override 
    public Student bind(String key, String value) {
        return findByEan(value);
    }

    @Override
    public String unbind(String key) {
        return ean;
    }

    @Override
    public String javascriptUnbind() {
        return ean;
    }
}