package controllers;

import com.avaje.ebean.Page;
import models.Teacher;
import models.Tag;
import models.UserAccount;
import models.Student;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.teachers.details;
import views.html.teachers.dashboardTeacher;

import com.avaje.ebean.Ebean;
import java.util.ArrayList;
import java.util.List;
import play.mvc.Security;

import java.util.regex.Pattern;

/**
 * Created by QuanByn on 2/2/2015.
 */
@Security.Authenticated(Secured.class)
public class Teachers extends Controller {
    private static final Form<Teacher> teacherForm = Form.form(Teacher.class);

    public static Result list(Integer page) {
        UserAccount useracc = UserAccount.findByEmail(session().get("email"));
        Student student = Student.findByEmail(session().get("email"));
        if (useracc == null && student == null){
            return redirect(routes.Teachers.details(Teacher.findByEmail(session().get("email"))));
        }
        Page<Teacher> teachers = Teacher.find(page);
        return ok(views.html.TeacherCatalog.render(teachers));
    }

    public static Result newTeacher(){
        return ok(details.render(teacherForm));
    }
    public static Result details(Teacher teacher){
        Student student = Student.findByEmail(session().get("email"));
        if(student != null){
            return redirect(routes.Teachers.list(0));
        }
        if(teacher == null){
            return notFound(String.format("teacher %s does not exist.",teacher.ean));
        }
        Form<Teacher> filledForm = teacherForm.fill(teacher);

        return ok(details.render(filledForm));
    }

    public static Result save() {
        Form<Teacher> boundForm = teacherForm.bindFromRequest();
        if(boundForm.hasErrors()){
            flash("error","Vui lòng hoàn thành theo yêu cầu !");
            return badRequest(details.render(boundForm));
        }
        //ktra confirm_password
        if(!boundForm.field("password").valueOr("").isEmpty()){
            if(!boundForm.field("password").valueOr("").equals(boundForm.field("confirm_password").value())){
                boundForm.reject("confirm_password","Password don't match");
            }
        }
        Teacher teacher = boundForm.get();
        teacher.ean = teacher.ean.toUpperCase();
        if (UserAccount.findByEmail(teacher.email) != null) {
            flash("error", "Địa chỉ email này trùng với tài khoản giáo vụ!");
            return badRequest(details.render(boundForm));
        }
        if (Teacher.findByEmail(teacher.email) != null) {
            flash("error", "Địa chỉ email trùng với tài khoản giảng viên!");
            return badRequest(details.render(boundForm));
        }
        if (teacher.id == null) {
            if (Teacher.findByEan(teacher.ean) != null) {
                flash("error", "Mã số sinh viên đã tồn tại !");
                return badRequest(details.render(boundForm));
            }
            if (Student.findByEmail(teacher.email) != null) {
                flash("error", "Email đã tồn tại !");
                return badRequest(details.render(boundForm));
            }
        } else {
            List<Student> students = Student.findAll();
            for (int i = 0; i < students.size(); i++) {
                if (students.get(i).ean.equals(teacher.ean) &&
                        !students.get(i).id.equals(teacher.id)) {
                    flash("error", "Mã số sinh viên đã tồn tại !");
                    return badRequest(details.render(boundForm));
                }
                if (students.get(i).email.equals(teacher.email) &&
                        !students.get(i).id.equals(teacher.id)) {
                    flash("error", "Email đã tồn tại !");
                    return badRequest(details.render(boundForm));
                }
            }
        }
        if (teacher.id == null){
            teacher.save();
        } else {
            teacher.update();
        }
        flash("success",String.format("Đã thêm thành công giáo viên %s",teacher));
        return redirect(routes.Teachers.list(0));
    }

    public static Result delete(String ean){
        final Teacher teacher = Teacher.findByEan(ean);
        if(teacher == null){
            return notFound(String.format("teacher %s does not exist.", ean));
        }
        teacher.delete();
        return redirect(routes.Teachers.list(0));
    }
    public static Result dashboard(Teacher teacher){
        return ok(views.html.teachers.dashboardTeacher.render(teacher));
    }
}
