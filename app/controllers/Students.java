package controllers;
import play.twirl.api.Html;
import com.avaje.ebean.Page;
import models.Student;
import models.Tag;
import models.Teacher;
import models.UserAccount;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;
import views.html.students.details;
import views.html.students.dashboardStudent;
import com.avaje.ebean.Ebean;
import java.util.ArrayList;
import java.util.List;
import play.mvc.Security;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;
import play.mvc.*;
import static play.data.Form.form;
/**
 * Created by QuanByn on 2/2/2015.
 */
@Security.Authenticated(Secured.class)
public class Students extends Controller {
    private static final Form<Student> studentForm = Form.form(Student.class);

    public static Result list(Integer page) {
        UserAccount useracc = UserAccount.findByEmail(session().get("email"));
        Teacher teacher = Teacher.findByEmail(session().get("email"));
        if (useracc == null && teacher == null){
            return redirect(routes.Students.details(Student.findByEmail(session().get("email"))));
        }
        Page<Student> students = Student.find(page);
        return ok(views.html.catalog.render(students));
    }

    public static Result newStudent(){
        return ok(details.render(studentForm));
    }
    public static Result details(Student student){
        Teacher teacher = Teacher.findByEmail(session().get("email"));
        if(teacher != null){
            return redirect(routes.Students.list(0));
        }
        if(student == null){
            return notFound(String.format("student %s does not exist.",student.ean));
        }
        Form<Student> filledForm = studentForm.fill(student);

        return ok(details.render(filledForm));
    }
    public static Result detailUser(Student student){
        if (student == null){
            return notFound(String.format("Student %s does not exist.",student.ean));
        }
        Form<Student> filledForm = studentForm.fill(student);

        return ok(details.render(filledForm));
    }

    public static Result save() {
        Form<Student> boundForm = studentForm.bindFromRequest();
        if(boundForm.hasErrors()){
            flash("error","Vui lòng hoàn thành theo yêu cầu !");
            return badRequest(details.render(boundForm));
        }
        //ktra confirm_password
        if(!boundForm.field("password").valueOr("").isEmpty()){
            if(!boundForm.field("password").valueOr("").equals(boundForm.field("confirm_password").value())){
                boundForm.reject("confirm_password","Password don't match");
            }
        }
        Student student = boundForm.get();
        student.ean = student.ean.toUpperCase();
        if (UserAccount.findByEmail(student.email) != null) {
            flash("error", "Địa chỉ email này trùng với tài khoản giáo vụ!");
            return badRequest(details.render(boundForm));
        }
        if (Teacher.findByEmail(student.email) != null) {
            flash("error", "Địa chỉ email trùng với tài khoản giảng viên!");
            return badRequest(details.render(boundForm));
        }
        // ktra mã sinh viên xem có hợp lệ không
        if(!boundForm.field("ean").valueOr("").isEmpty()){
            String temp= boundForm.field("ean").value();

            Pattern pattern = Pattern.compile(".*[^0-9].*");
            Pattern digitPattern = Pattern.compile("\\d{8}");

            if(digitPattern.matcher(temp).matches() == false){
                boundForm.reject("ean", "Mã sinh viên phải có 8 kí tự , yêu cầu nhập lại");
            }

            if(pattern.matcher(temp).matches() == true ){
                boundForm.reject("ean", "Mã sinh viên phải là số từ [0->9], yêu cầu nhập lại");
            }
        }
        if (student.id == null) {
            if (Student.findByEan(student.ean) != null) {
                flash("error", "Mã số sinh viên đã tồn tại !");
                return badRequest(details.render(boundForm));
            }
            if (Student.findByEmail(student.email) != null) {
                flash("error", "Email đã tồn tại !");
                return badRequest(details.render(boundForm));
            }
        } else {
            List<Student> students = Student.findAll();
            for (int i = 0; i < students.size(); i++) {
                if (students.get(i).ean.equals(student.ean) &&
                        !students.get(i).id.equals(student.id)) {
                    flash("error", "Mã số sinh viên đã tồn tại !");
                    return badRequest(details.render(boundForm));
                }
                if (students.get(i).email.equals(student.email) &&
                        !students.get(i).id.equals(student.id)) {
                    flash("error", "Email đã tồn tại !");
                    return badRequest(details.render(boundForm));
                }
            }
        }
        List<Tag> tags = new ArrayList<Tag>();
        for (Tag tag : student.tags) {
            if(tag.id != null) {
                tags.add(Tag.findById(tag.id));
            }
        }
        student.tags = tags;
        if (student.id == null){
            student.save();
        } else {
            student.update();
        }
        flash("success",String.format("Đã thêm thành công sinh viên %s",student));
        return redirect(routes.Students.list(0));
    }

    public static Result delete(String ean){
        final Student student = Student.findByEan(ean);
        if(student == null){
            return notFound(String.format("student %s does not exist.", ean));
        }
        student.delete();
        return redirect(routes.Students.list(0));
    }
    public static Result dashboard(Student student){
        return ok(views.html.students.dashboardStudent.render(student));
    }

    public static class UploadAvatarForm {

        public Http.MultipartFormData.FilePart avatar;

        public String validate() {
            Http.MultipartFormData data = request().body().asMultipartFormData();
            avatar = data.getFile("avatar");
            if (avatar == null) {
                return "Tập tin ảnh bị thiếu !";
            }
            return null;
        }
    }

    public static Result upload() {
        return ok(views.html.students.uploadavatar.render(form(UploadAvatarForm.class)));
    }

    public static Result uploadAvatar() {
        Form<UploadAvatarForm> form = form(UploadAvatarForm.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(views.html.students.uploadavatar.render(form));
        }
        Student student = Student.findByEmail(session().get("email"));
        student.avatar = new byte[(int) form.get().avatar.getFile().length()];
        InputStream inputStream = null;
        try {
            inputStream = new BufferedInputStream(new FileInputStream(form.get().avatar.getFile()));
            inputStream.read(student.avatar);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        student.update();
        flash("success", "Đã cập nhật ảnh đại diện !");
        return redirect(routes.Students.dashboard(Student.findByEmail(session().get("email"))));
    }


    public static Result getAvatar(long id) {
        Student student = Student.find.byId(id);
        if (student != null) {
            return ok(student.avatar).as("avatar");
        } else {
            flash("error", "Ảnh đại diện không tồn tại!");
            return redirect(routes.Students.dashboard(Student.findByEmail(session().get("email"))));
        }
    }
}
