package controllers;

import models.UserAccount;
import models.Student;
import models.Teacher;
import play.data.Form;
import play.mvc.*;
import static play.data.Form.form;
import views.html.*;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }
    public static Result contact() {
        return ok(views.html.contact.render());
    }
     public static Result login() {
       return ok(
          login.render(form(Login.class))
       );
    }
    public static Result authenticate() {
        Form<Login> loginForm = form(Login.class).bindFromRequest();
        String email = loginForm.get().email;
        String password = loginForm.get().password;
 
        session().clear();
        if (UserAccount.authenticate(email, password) != null) {
              session("email", email);
              return redirect(routes.Application.admin());
        }else if (Student.authenticate(email, password) != null) {
              session("email", email);
              return redirect(routes.Students.dashboard(Student.findByEmail(email)));
        }else if (Teacher.authenticate(email,password) != null) {
              session("email", email);
              return redirect(routes.Teachers.dashboard(Teacher.findByEmail(email)));
        }
        flash("error", "Invalid email and/or password");
        return redirect(routes.Application.login());  
    }
    public static Result logout() {
       session().clear();
       return redirect(routes.Application.index());
    }
    public static class Login {
        public String email;
        public String password;
    }
    public static Result admin() {
      if (session().get("email") == null) {
            return redirect(routes.Application.index());
      }
      if (UserAccount.findByEmail(session().get("email")) == null) {
        if(Student.findByEmail(session().get("email")) == null){
          return redirect(routes.Teachers.dashboard(Teacher.findByEmail(session().get("email"))));
        } else {
          return redirect(routes.Students.dashboard(Student.findByEmail(session().get("email"))));
        }
      }
      return ok(views.html.dashboard.render());
    }
}
