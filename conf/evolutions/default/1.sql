# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table student (
  id                        bigint not null,
  ean                       varchar(255),
  name                      varchar(255),
  birthday                  timestamp,
  address                   varchar(255),
  phone                     varchar(255),
  faculty                   varchar(255),
  classes                   varchar(255),
  email                     varchar(255),
  password                  varchar(255),
  confirm_password          varchar(255),
  avatar                    bytea,
  constraint pk_student primary key (id))
;

create table tag (
  id                        bigint not null,
  name                      varchar(255),
  constraint pk_tag primary key (id))
;

create table teacher (
  id                        bigint not null,
  ean                       varchar(255),
  name                      varchar(255),
  phone                     varchar(255),
  faculty                   varchar(255),
  classes                   varchar(255),
  email                     varchar(255),
  password                  varchar(255),
  confirm_password          varchar(255),
  picture                   bytea,
  constraint pk_teacher primary key (id))
;

create table user_account (
  id                        bigint not null,
  email                     varchar(255),
  password                  varchar(255),
  constraint pk_user_account primary key (id))
;


create table student_tag (
  student_id                     bigint not null,
  tag_id                         bigint not null,
  constraint pk_student_tag primary key (student_id, tag_id))
;

create table teacher_tag (
  teacher_id                     bigint not null,
  tag_id                         bigint not null,
  constraint pk_teacher_tag primary key (teacher_id, tag_id))
;
create sequence student_seq;

create sequence tag_seq;

create sequence teacher_seq;

create sequence user_account_seq;




alter table student_tag add constraint fk_student_tag_student_01 foreign key (student_id) references student (id);

alter table student_tag add constraint fk_student_tag_tag_02 foreign key (tag_id) references tag (id);

alter table teacher_tag add constraint fk_teacher_tag_teacher_01 foreign key (teacher_id) references teacher (id);

alter table teacher_tag add constraint fk_teacher_tag_tag_02 foreign key (tag_id) references tag (id);

# --- !Downs

drop table if exists student cascade;

drop table if exists student_tag cascade;

drop table if exists tag cascade;

drop table if exists teacher_tag cascade;

drop table if exists teacher cascade;

drop table if exists user_account cascade;

drop sequence if exists student_seq;

drop sequence if exists tag_seq;

drop sequence if exists teacher_seq;

drop sequence if exists user_account_seq;

