name := """managerstudentsystem"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq (
  javaEbean,
  "org.postgresql" % "postgresql" % "9.3-1100-jdbc41",
  filters
)

libraryDependencies += javaEbean